/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BattleShips;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author rapha
 */
public class Spielfeld {

    private Schiff BS, U1, U2, U3, DD1, DD2, CA;
    private Schiff EBS, EU1, EU2, EU3, EDD1, EDD2, ECA;
    private ArrayList<Schiff> playerShips;
    private ArrayList<Schiff> enemyShips;
    private char[][] spielfeld;
    private char[][] gegnerfeld;
    char[] letters;
    private final int numCols;
    private final int numRows;
    Spieler p1, enemy;
    Random random;

    public Spielfeld() {
        random = new Random();
        numCols = 10;
        numRows = 10;
        letters = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
        spielfeld = initializeField();
        gegnerfeld = initializeField();
        playerShips = new ArrayList<>();
        enemyShips = new ArrayList<>();
        //Player Ships, can set location manually
        BS = new Schiff("Battleship", 5);
        CA = new Schiff("Cruiser", 4);
        DD1 = new Schiff("Destroyer 1", 3);
        DD2 = new Schiff("Destroyer 2", 3);
        U1 = new Schiff("Sub 1", 2);
        U2 = new Schiff("Sub 2", 2);
        U3 = new Schiff("Sub 3", 2);
        playerShips.add(BS);
        playerShips.add(CA);
        playerShips.add(DD1);
        playerShips.add(DD2);
        playerShips.add(U1);
        playerShips.add(U2);
        playerShips.add(U3);
        setShips();
        p1 = new Spieler("P1", 1, playerShips);

        // Enemy Ships (randomly placed)
        EBS = new Schiff("Battleship", 5);
        ECA = new Schiff("Cruiser", 4);
        EDD1 = new Schiff("Destroyer 1", 3);
        EDD2 = new Schiff("Destroyer 2", 3);
        EU1 = new Schiff("Sub 1", 2);
        EU2 = new Schiff("Sub 2", 2);
        EU3 = new Schiff("Sub 3", 2);
        enemyShips.add(EBS);
        enemyShips.add(ECA);
        enemyShips.add(EDD1);
        enemyShips.add(EDD2);
        enemyShips.add(EU1);
        enemyShips.add(EU2);
        enemyShips.add(EU3);
        setEnemyShips();
        enemy = new Spieler("AI", 2, enemyShips);

    }

    private char[][] initializeField() {
        char[][] area = new char[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                area[i][j] = ' ';
            }
        }
        return area;
    }

    public void printBothFields() {
        System.out.println("Enemy Ships: \n");
        printEnemyField();
        System.out.println("\nYour ships:");
        printPlayerFeld();
    }

    public void printPlayerFeld() {
        StringBuilder SB = new StringBuilder();
        SB.append("    1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10  \n");
        for (int i = 0; i < 10; i++) {
            SB.append(letters[i]);
            SB.append(" | ");
            for (int j = 0; j < 10; j++) {
                SB.append(spielfeld[i][j]);
                SB.append(" | ");
            }
            SB.append("\n");
        }
        System.out.println(SB.toString());
    }

    public void printEnemyField() {
        StringBuilder SB = new StringBuilder();
        SB.append("    1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10  \n");
        for (int i = 0; i < 10; i++) {
            SB.append(letters[i]);
            SB.append(" | ");
            for (int j = 0; j < 10; j++) {
                if (gegnerfeld[i][j] == 'S') {
                    SB.append(" ");
                    SB.append(" | ");
                } else {
                    SB.append(gegnerfeld[i][j]);
                    SB.append(" | ");
                }
            }
            SB.append("\n");
        }
        System.out.println(SB.toString());
    }

    public void askShipLocation(Schiff s) {
        printPlayerFeld();
        Schiff ship = s;
        System.out.println("Where would you like to place your " + s.getName() + " ?\n It has a length of " + s.getLength() + " units.");
        System.out.println("Please enter locations in EXACTLY this format: LETTER-NUMBER, or A-1");
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Starting location");
        String out = sc.next();
        int[] coords = translateInstruction(out);
        System.out.println("Enter which way the ship should face:\nN E S W");
        String direction = sc.next();
        boolean ok = checkShipLocation(coords, s, direction);
        if (!ok) {
            System.out.println("Invalid direction selected.");
            askShipLocation(ship);
        } else {
            setSingleShip(ship, direction, coords);
        }
    }

    public void setShips() {
        for (Schiff s : playerShips) {
            try {
                askShipLocation(s);
            } catch (Exception e) {
                System.out.println("You entered incorrect data. Please try again.");
                askShipLocation(s);
            }
        }
        System.out.println("Success! You've placed all your ships.");
    }

    public String randomDir() {
        String dir = "";
        int randDir = random.nextInt(4);
        switch (randDir) {
            case 0:
                dir = "n";
                break;
            case 1:
                dir = "e";
                break;
            case 2:
                dir = "w";
                break;
            case 3:
                dir = "s";
                break;
        }
        return dir;
    }

    public int[] randomCoords() {
        int x = random.nextInt(10);
        int y = random.nextInt(10);
        int[] coords = new int[]{y, x};
        return coords;
    }

    public void setEnemyShips() {
        String dir = "";
        boolean set;
        int[] coords;
        for (Schiff s : enemyShips) {
            coords = randomCoords();
            dir = randomDir();
            set = checkShipLocation(coords, s, dir);
            while (!set) {
                coords = randomCoords();
                dir = randomDir();
                set = checkShipLocation(coords, s, dir);
                System.out.println("Enemy can't place ship...");
            }
            setSingleEnemyShip(s, dir, coords);
            System.out.println("Enemy placed ship.");
        }

    }

    public void setSingleShip(Schiff s, String direction, int[] coords) {
        int x = coords[0];
        int y = coords[1];
        int len = s.getLength();
        String dir = direction.toLowerCase();

        // Zelle muss leer sein
        for (int i = 0; i < len; i++) {
            switch (dir) {
                case "n":
                    spielfeld[x - i][y] = 'S';
                    break;
                case "e":
                    spielfeld[x][y + i] = 'S';
                    break;
                case "s":
                    spielfeld[x + i][y] = 'S';
                    break;
                case "w":
                    spielfeld[x][y - i] = 'S';
                    break;
            }
        }
    }

    public void setSingleEnemyShip(Schiff s, String direction, int[] coords) {
        int x = coords[0];
        int y = coords[1];
        int len = s.getLength();
        String dir = direction.toLowerCase();

        // Zelle muss leer sein
        for (int i = 0; i < len; i++) {
            switch (dir) {
                case "n":
                    gegnerfeld[x - i][y] = 'S';
                    break;
                case "e":
                    gegnerfeld[x][y + i] = 'S';
                    break;
                case "s":
                    gegnerfeld[x + i][y] = 'S';
                    break;
                case "w":
                    gegnerfeld[x][y - i] = 'S';
                    break;
            }
        }
    }

    private boolean checkShipLocation(int[] coords, Schiff s, String direction) {
        int x = coords[0];
        int y = coords[1];
        int len = s.getLength();
        int checker = 0;

        // Zelle muss leer sein
        if (spielfeld[x][y] == ' ') {
            // Prüfe, ob Richtung das Spielfeld verlässt, oder mit einem anderen Schiff überlappt
            try {
                for (int i = 0; i < len; i++) {
                    switch (direction.toLowerCase()) {
                        case "n":
                            if (x - len < 0) {
                                return false;
                            }
                            if (spielfeld[x - i][y] == ' ') {
                                checker++;
                            }
                            break;
                        case "e":
                            if (y + len > 10) {
                                return false;
                            }
                            if (spielfeld[x][y + i] == ' ') {
                                checker++;
                            }
                            break;
                        case "s":
                            if (len + x > 10) {
                                return false;
                            }
                            if (spielfeld[x + i][y] == ' ') {
                                checker++;
                            }
                            break;
                        case "w":
                            if (y - len < 0) {
                                return false;
                            }
                            if (spielfeld[x][y - i] == ' ') {
                                checker++;
                            }
                            break;
                    }
                    if (checker >= len) {
                        return true;
                    }
                }
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    public boolean openFire(int[] coords) {
        int y = coords[1];
        int x = coords[0];
        if (spielfeld[x][y] == 'S') {
            System.out.println("HIT!");
            gegnerfeld[x][y] = '*';
            printPlayerFeld();
            return true;
        } else {
            System.out.println("MISS!");
            gegnerfeld[x][y] = 'O';
            printPlayerFeld();
            return false;
        }
    }

    public boolean enemyFire() {
        int[] coords = randomCoords();
        int y = coords[1];
        int x = coords[0];
        if (spielfeld[x][y] == 'S') {
            System.out.println("ENEMY HIT!");
            spielfeld[x][y] = '*';
//            printPlayerFeld();
            return true;
        } else {
            System.out.println("ENEMY MISS!");
            spielfeld[x][y] = 'O';
//            printPlayerFeld();
            return false;
        }
    }

    private int[] translateInstruction(String instuction) {
        int y = 999;
        String xS = Character.toString(instuction.charAt(2));
        int x = Integer.valueOf(xS);
        String yS = Character.toString(instuction.charAt(0));
        yS = yS.toLowerCase();
        switch (yS) {
            case "a":
                y = 0;
                break;
            case "b":
                y = 1;
                break;
            case "c":
                y = 2;
                break;
            case "d":
                y = 3;
                break;
            case "e":
                y = 4;
                break;
            case "f":
                y = 5;
                break;
            case "g":
                y = 6;
                break;
            case "h":
                y = 7;
                break;
            case "i":
                y = 8;
                break;
            case "j":
                y = 9;
                break;
        }
        x--;
        System.out.println("x: " + x);
        System.out.println("y: " + y);
        int[] coords = {y, x};
        return coords;
    }

    public int[] askForCoords() {
        System.out.println("Enter the coordinates of your shot!");
        Scanner sc = new Scanner(System.in);
        String s = sc.next();
        int[] coords = translateInstruction(s);
        return coords;
    }

    public ArrayList<Schiff> getPlayerShips() {
        return playerShips;
    }

    public ArrayList<Schiff> getEnemyShips() {
        return enemyShips;
    }

}
