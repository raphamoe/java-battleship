/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BattleShips;

import java.util.ArrayList;

/**
 *
 * @author rapha
 */
public class Spieler {

    private String name;
    private int spielerNr;
    private ArrayList<Schiff> schiffe;
    private int hitpoints;

    public Spieler(String name, int spielerNr, ArrayList<Schiff> schiffe) {
        this.name = name;
        this.spielerNr = spielerNr;
        this.schiffe = schiffe;
    }

    public String getName() {
        return name;
    }

    public int getSpielerNr() {
        return spielerNr;
    }

    public ArrayList<Schiff> getSchiffe() {
        return schiffe;
    }

    public void setHitpoints(int hitpoints) {
        this.hitpoints = hitpoints;
    }

    public int getHitpoints() {
        return hitpoints;
    }

 

}
