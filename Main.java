/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BattleShips;

import java.util.ArrayList;

/**
 *
 * @author rapha
 */
public class Main {

    public static void main(String[] args) {
        run();
    }

    public static void run() {
        Spielfeld spiel = new Spielfeld();
        boolean running = true;
        int playerHitpoints = calcHitpoints(spiel.getPlayerShips());
        int enemyHitpoints = calcHitpoints(spiel.getEnemyShips());
        while (running) {
            System.out.println("Enemy turn.");
            if (spiel.enemyFire()) {
                playerHitpoints--;
            }
            if (playerHitpoints == 0) {
                System.out.println("Game Over! You Lose!");
                running = false;
            }
            spiel.printBothFields();
            System.out.println("Your turn!");
            try {
                int[] coords = spiel.askForCoords();
                if (spiel.openFire(coords)) {
                    enemyHitpoints--;
                }
            } catch (Exception e) {
                System.out.println("Entered incorrect Data! Plase follow this format:\n Letter-Number, like A-1.\n "
                        + "Since I am too lazy to program proper error handling, your turn is now forfeit!");
            }
            if (enemyHitpoints == 0) {
                System.out.println("Game Over! You Win!");
                running = false;
            }

        }
    }

    public static int calcHitpoints(ArrayList<Schiff> ships) {
        int hp = 0;
        for (Schiff s : ships) {
            hp += s.getLength();
        }
        return hp;
    }

}
